<?php
declare(strict_types=1);

namespace N11t\Holidays\Entity;

use N11t\AbstractCollection\AbstractCollection;

class HolidayCollection extends AbstractCollection
{

    public function __construct(Holiday ...$holidays)
    {
        $this->values = $holidays;
    }
}

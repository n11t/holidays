<?php
declare(strict_types=1);

namespace N11t\Holidays\Entity;

class Holiday
{

    /**
     * @var \DateTimeImmutable
     */
    private $date;

    /**
     * @var string
     */
    private $name;

    /**
     * Holiday constructor.
     * @param string $date
     * @param string $name
     */
    public function __construct(string $date, string $name)
    {
        $this->date = new \DateTimeImmutable($date);
        $this->name = $name;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}

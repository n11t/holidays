<?php
declare(strict_types=1);

namespace N11t\Holidays;

use N11t\Holidays\Entity\HolidayCollection;

interface HolidayCalculator
{

    /**
     * Check if given Date is holiday or not.
     *
     * @param \DateTimeInterface $date The Date to check.
     * @return bool True if given Date is holiday. False if not.
     */
    public function isHoliday(\DateTimeInterface $date): bool;

    /**
     * Get all holidays between given dates.
     *
     * @param \DateTimeInterface $start The start date.
     * @param \DateTimeInterface $end The end date.
     * @return HolidayCollection
     */
    public function between(\DateTimeInterface $start, \DateTimeInterface $end): HolidayCollection;
}

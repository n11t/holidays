<?php
declare(strict_types=1);

namespace N11t\Holidays\Calculator;

use N11t\Holidays\Calculator\Provider\HolidayProvider;
use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;
use N11t\Holidays\HolidayCalculator;

abstract class Calculator implements HolidayCalculator
{

    /**
     * @var HolidayProvider[]
     */
    private $providers;

    /**
     * Calculator constructor.
     */
    public function __construct()
    {
        $this->providers = [];
    }

    /**
     * Get all holidays for given year.
     *
     * @param int $year
     * @return HolidayCollection
     */
    abstract protected function getHolidays(int $year): HolidayCollection;

    /**
     * Add a new provider.
     *
     * @param HolidayProvider $provider
     */
    public function addProvider(HolidayProvider $provider): void
    {
        $this->providers[] = $provider;
    }

    /**
     * Get the easter date
     *
     * @param int $year
     * @return \DateTimeImmutable
     */
    public function getEasterDate(int $year): \DateTimeImmutable
    {
        $baseDate = sprintf('%d-03-21 00:00:00', $year);
        $base = new \DateTimeImmutable($baseDate);

        $days = \easter_days($year);

        $dateInterval = sprintf('+ %d days', $days);
        return $base->modify($dateInterval);
    }

    /**
     * {@inheritdoc}
     */
    public function isHoliday(\DateTimeInterface $date): bool
    {
        $holidays = $this->between($date, $date);

        return \count($holidays) > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function between(\DateTimeInterface $start, \DateTimeInterface $end): HolidayCollection
    {
        $start = new \DateTimeImmutable($start->format('Y-m-d 00:00:00'));
        $end = new \DateTimeImmutable($end->format('Y-m-d 00:00:00'));

        $startYear = (int)$start->format('Y');
        $endYear = (int)$end->format('Y');

        $holidays = $this->getHolidaysBetweenYears($startYear, $endYear);

        $filteredHolidays = array_filter($holidays->toArray(), function (Holiday $holiday) use ($start, $end) {
            $date = $holiday->getDate();

            return $date >= $start && $end >= $date;
        });

        return new HolidayCollection(...$filteredHolidays);
    }

    /**
     * Get all holidays between the given years.
     *
     * @param int $startYear The year to start with.
     * @param int $endYear The year to end with.
     * @return HolidayCollection
     */
    private function getHolidaysBetweenYears(int $startYear, int $endYear): HolidayCollection
    {
        $holidays = [];
        for ($i = $startYear; $i <= $endYear; $i++) {
            $holidays[] = $this->getHolidays($i)->toArray();

            /** @var HolidayProvider $provider */
            foreach ($this->providers as $provider) {
                $holidays[] = $provider->getHolidays($i)->toArray();
            }
        }

        // We use this to prevent array_merge in loops.
        return new HolidayCollection(...\array_merge(...$holidays));
    }

    /**
     * Clone the DateTime and modify it.
     *
     * Result is a date in Y-m-d to create Holiday with.
     *
     * @param \DateTimeImmutable $toClone The DateTime to clone.
     * @param string $modifier The modifier to modify the date time with.
     * @return string The new date in Y-m-d format.
     */
    protected function modifyAndFormat(\DateTimeImmutable $toClone, string $modifier): string
    {
        $dateTime = $toClone->modify($modifier);

        return $dateTime->format('Y-m-d');
    }
}

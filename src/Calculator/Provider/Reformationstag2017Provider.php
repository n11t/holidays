<?php
declare(strict_types=1);

namespace N11t\Holidays\Calculator\Provider;

use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;

class Reformationstag2017Provider implements HolidayProvider
{

    /**
     * {@inheritdoc}
     */
    public function getHolidays(int $year): HolidayCollection
    {
        $holidays = [];
        if ($year === 2017) {
            $holidays[] = new Holiday('2017-10-31', 'Reformationstag');
        }

        return new HolidayCollection(...$holidays);
    }
}

<?php
declare(strict_types=1);

namespace N11t\Holidays\Calculator\Provider;

use N11t\Holidays\Entity\HolidayCollection;

interface HolidayProvider
{

    /**
     * Get all holidays for given year.
     *
     * @param int $year
     * @return HolidayCollection
     */
    public function getHolidays(int $year): HolidayCollection;
}

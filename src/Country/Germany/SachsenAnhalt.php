<?php
declare(strict_types=1);

namespace N11t\Holidays\Country\Germany;

use N11t\Holidays\Country\Germany;
use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;

class SachsenAnhalt extends Germany
{

    protected function getHolidays(int $year): HolidayCollection
    {
        $holidays = parent::getHolidays($year)->toArray();

        $holidays[] = new Holiday("$year-01-06", 'Heilige Drei Könige');
        $holidays[] = new Holiday("$year-10-31", 'Reformationstag');

        return new HolidayCollection(...$holidays);
    }
}

<?php
declare(strict_types=1);

namespace N11t\Holidays\Country\Germany;

use N11t\Holidays\Country\Germany;
use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;

class Berlin extends Germany
{
    protected function getHolidays(int $year): HolidayCollection
    {
        $holidays = parent::getHolidays($year)->toArray();

        if ($year > 2018) {
            $holidays[] = new Holiday("$year-03-08", 'Weltfrauentag');
        }

        return new HolidayCollection(...$holidays);
    }
}

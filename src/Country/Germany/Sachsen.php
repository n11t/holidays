<?php
declare(strict_types=1);

namespace N11t\Holidays\Country\Germany;

use N11t\Holidays\Country\Germany;
use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;

class Sachsen extends Germany
{

    protected function getHolidays(int $year): HolidayCollection
    {
        $holidays = parent::getHolidays($year)->toArray();

        $holidays[] = new Holiday("$year-10-31", 'Reformationstag');
        $holidays[] = new Holiday("$year-11-23 last wed", 'Buß- und Bettag');

        return new HolidayCollection(...$holidays);
    }
}

<?php
declare(strict_types=1);

namespace N11t\Holidays\Country\Germany;

use N11t\Holidays\Country\Germany;
use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;

class Brandenburg extends Germany
{

    protected function getHolidays(int $year): HolidayCollection
    {
        $holidays = parent::getHolidays($year)->toArray();

        $easter = $this->getEasterDate($year);
        $holidays[] = new Holiday($easter->format('Y-m-d'), 'Ostersonntag');

        $holidays[] = new Holiday($this->modifyAndFormat($easter, '+49 day'), 'Pfingstsonntag');

        $holidays[] = new Holiday("$year-10-31", 'Reformationstag');

        return new HolidayCollection(...$holidays);
    }
}

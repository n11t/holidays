<?php
declare(strict_types=1);

namespace N11t\Holidays\Country\Germany;

use N11t\Holidays\Country\Germany;
use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;

class Hessen extends Germany
{

    protected function getHolidays(int $year): HolidayCollection
    {
        $holidays = parent::getHolidays($year)->toArray();

        $easter = $this->getEasterDate($year);
        $holidays[] = new Holiday($this->modifyAndFormat($easter, '+60 day'), 'Frohnleichnam');

        return new HolidayCollection(...$holidays);
    }
}

<?php
declare(strict_types=1);

namespace N11t\Holidays\Country;

use N11t\Holidays\Calculator\Calculator;
use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;

class Germany extends Calculator
{

    /**
     * {@inheritdoc}
     */
    protected function getHolidays(int $year): HolidayCollection
    {
        $holidays = [];

        $easter = $this->getEasterDate($year);

        $holidays[] = new Holiday($this->modifyAndFormat($easter, '-2 days'), 'Karfreitag');
        $holidays[] = new Holiday($this->modifyAndFormat($easter, '+1 day'), 'Ostermontag');
        $holidays[] = new Holiday($this->modifyAndFormat($easter, '+39 day'), 'Christi Himmelfahrt');
        $holidays[] = new Holiday($this->modifyAndFormat($easter, '+50 day'), 'Pfingstmontag');

        $holidays[] = new Holiday("$year-01-01", 'Neujahr');
        $holidays[] = new Holiday("$year-05-01", 'Tag der Arbeit');
        $holidays[] = new Holiday("$year-10-03", 'Tag der Deutschen Einheit');
        $holidays[] = new Holiday("$year-12-25", '1. Weihnachtstag');
        $holidays[] = new Holiday("$year-12-26", '2. Weihnachtstag');

        return new HolidayCollection(...$holidays);
    }
}

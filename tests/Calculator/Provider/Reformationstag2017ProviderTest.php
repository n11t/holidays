<?php
declare(strict_types=1);

namespace N11t\Holidays\Calculator\Provider;

use N11t\Holidays\Entity\Holiday;
use PHPUnit\Framework\TestCase;

class Reformationstag2017ProviderTest extends TestCase
{

    public function testCanAddReformationstag()
    {
        // Arrange
        $provider = new Reformationstag2017Provider();

        // Assert
        self::assertCount(0, $provider->getHolidays(2016));
        self::assertCount(1, $provider->getHolidays(2017));
        self::assertEquals([
            new Holiday('2017-10-31', 'Reformationstag'),
        ], $provider->getHolidays(2017)->toArray());
        self::assertCount(0, $provider->getHolidays(2019));
    }
}

<?php
declare(strict_types=1);

namespace N11t\Holidays\Calculator;

use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;
use N11t\Holidays\Fake\Calculator\FakeHolidayCalculator;
use N11t\Holidays\Fake\Calculator\Provider\FakeHolidayProvider;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{

    public function easterDateProvider(): array
    {
        return [
            [1999, '1999-04-04'],
            [1492, '1492-04-22'],
            [1913, '1913-03-23'],
            [2018, '2018-04-01'],
            [2323, '2323-03-25'],
        ];
    }

    /**
     * @dataProvider easterDateProvider
     *
     * @param int $year
     * @param string $expectedEasterDate
     */
    public function testCanCalculateEaster(int $year, string $expectedEasterDate)
    {
        // Act
        $actualEasterDate = (new FakeHolidayCalculator())->getEasterDate($year);

        // Assert
        self::assertSame($expectedEasterDate, $actualEasterDate->format('Y-m-d'));
    }

    public function testWillSetEasterTimeToMidnight()
    {
        // Act
        $actualEasterDate = (new FakeHolidayCalculator())->getEasterDate(2018);

        // Assert
        $actualTime = $actualEasterDate->format('His');
        self::assertSame('000000', $actualTime);
    }

    public function testCanIdentifyHoliday()
    {
        // Arrange
        $holidays = [
            new Holiday('2018-03-30', 'Karfreitag'),
            new Holiday('2018-12-24', 'Weihnachten'),
            new Holiday('2018-01-01', 'Silvester'),
        ];
        $calculator = new FakeHolidayCalculator(new HolidayCollection(...$holidays));

        // Act
        /** @var Holiday $holiday */
        foreach ($holidays as $holiday) {
            $date = $holiday->getDate();
            $falseDate = (clone $date)->modify('+ 1 day');

            self::assertTrue(
                $calculator->isHoliday($date),
                sprintf('Expected %s to be true', $date->format('Y-m-d'))
            );
            self::assertFalse(
                $calculator->isHoliday($falseDate),
                sprintf('Expected %s to be false', $falseDate->format('Y-m-d'))
            );
        }
    }

    public function testCanCalculateHolidaysBetween()
    {
        // Arrange
        $start = new \DateTime('2018-03-29');
        $end = new \DateTime('2019-04-30');

        $holidays = [
            new Holiday('2018-03-30', 'Karfreitag'),
            new Holiday('2018-04-01', 'Ostersonntag'),
            new Holiday('2018-04-02', 'Ostermontag'),
            new Holiday('2019-03-30', 'Karfreitag'),
            new Holiday('2019-04-01', 'Ostersonntag'),
            new Holiday('2019-04-02', 'Ostermontag'),
            new Holiday('2019-05-01', 'Tag der Arbeit'),
        ];
        $calculator = new FakeHolidayCalculator(new HolidayCollection(...$holidays));

        // Act
        $holidays = $calculator->between($start, $end);

        // Assert
        self::assertCount(6, $holidays);
        self::assertEquals([
            new Holiday('2018-03-30', 'Karfreitag'),
            new Holiday('2018-04-01', 'Ostersonntag'),
            new Holiday('2018-04-02', 'Ostermontag'),
            new Holiday('2019-03-30', 'Karfreitag'),
            new Holiday('2019-04-01', 'Ostersonntag'),
            new Holiday('2019-04-02', 'Ostermontag'),
        ], $holidays->toArray());
    }

    public function testCanUseHolidayProvider()
    {
        // Arrange
        $calculator = new FakeHolidayCalculator();
        $holidays = [
            new Holiday('2017-10-31', 'Reformationstag'),
        ];
        $calculator->addProvider(new FakeHolidayProvider(new HolidayCollection(...$holidays)));

        // Act
        $holidays = $calculator->between(new \DateTime('2017-01-01'), new \DateTime('2017-12-31'));

        // Assert
        self::assertCount(1, $holidays);
        self::assertEquals([
            new Holiday('2017-10-31', 'Reformationstag'),
        ], $holidays->toArray());
        $isHoliday = $calculator->isHoliday(new \DateTime('2017-10-31'));
        self::assertTrue($isHoliday, 'Expected Reformationstag 2017 to be holiday.');
    }
}

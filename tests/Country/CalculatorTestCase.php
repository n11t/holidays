<?php
declare(strict_types=1);

namespace N11t\Holidays\Country;

use N11t\Holidays\HolidayCalculator;
use PHPUnit\Framework\TestCase;

abstract class CalculatorTestCase extends TestCase
{

    abstract protected function getCalculatorInstance(): HolidayCalculator;

    abstract public function holidayProvider(): array;

    public function testInstanceOfHolidayCalculator()
    {
        $calculator = $this->getCalculatorInstance();

        self::assertInstanceOf(HolidayCalculator::class, $calculator);
    }

    /**
     * @dataProvider holidayProvider
     *
     * @param int $year
     * @param array $holidays
     */
    public function testCanCalculateHolidays(int $year, array $holidays)
    {
        // Arrange
        $calculator = $this->getCalculatorInstance();

        // Act
        foreach ($holidays as $holiday) {
            $date = new \DateTime($holiday[0]);
            $name = $holiday[1];

            self::assertTrue($calculator->isHoliday($date), "Expected $name [$holiday[0]] to be a holiday.");

            $collection = $calculator->between($date, $date);
            self::assertCount(1, $collection);
            self::assertSame($name, $collection->toArray()[0]->getName());
        }
        $start = new \DateTime("$year-01-01");
        $end = new \DateTime("$year-12-31");
        $actualHolidays = $calculator->between($start, $end);

        // Assert
        self::assertSameSize($holidays, $actualHolidays);
    }
}

<?php
declare(strict_types=1);

namespace N11t\Holidays\Country\Germany;

use N11t\Holidays\Country\CalculatorTestCase;
use N11t\Holidays\HolidayCalculator;

class HessenTest extends CalculatorTestCase
{

    protected function getCalculatorInstance(): HolidayCalculator
    {
        return new Hessen();
    }

    public function holidayProvider(): array
    {
        $result = [];
        $result[] = [
            2018,
            [
                ['2018-01-01', 'Neujahr'],
                ['2018-03-30', 'Karfreitag'],
                ['2018-04-02', 'Ostermontag'],
                ['2018-05-01', 'Tag der Arbeit'],
                ['2018-05-10', 'Christi Himmelfahrt'],
                ['2018-05-21', 'Pfingstmontag'],
                ['2018-05-31', 'Frohnleichnam'],
                ['2018-10-03', 'Tag der Deutschen Einheit'],
                ['2018-12-25', '1. Weihnachtstag'],
                ['2018-12-26', '2. Weihnachtstag'],
            ]
        ];
        $result[] = [
            2019,
            [
                ['2019-01-01', 'Neujahr'],
                ['2019-04-19', 'Karfreitag'],
                ['2019-04-22', 'Ostermontag'],
                ['2019-05-01', 'Tag der Arbeit'],
                ['2019-05-30', 'Christi Himmelfahrt'],
                ['2019-06-10', 'Pfingstmontag'],
                ['2019-06-20', 'Frohnleichnam'],
                ['2019-10-03', 'Tag der Deutschen Einheit'],
                ['2019-12-25', '1. Weihnachtstag'],
                ['2019-12-26', '2. Weihnachtstag'],
            ]
        ];
        $result[] = [
            2020,
            [
                ['2020-01-01', 'Neujahr'],
                ['2020-04-10', 'Karfreitag'],
                ['2020-04-13', 'Ostermontag'],
                ['2020-05-01', 'Tag der Arbeit'],
                ['2020-05-21', 'Christi Himmelfahrt'],
                ['2020-06-01', 'Pfingstmontag'],
                ['2020-06-11', 'Frohnleichnam'],
                ['2020-10-03', 'Tag der Deutschen Einheit'],
                ['2020-12-25', '1. Weihnachtstag'],
                ['2020-12-26', '2. Weihnachtstag'],
            ]
        ];

        return $result;
    }
}

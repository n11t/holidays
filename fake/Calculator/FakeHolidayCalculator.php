<?php
declare(strict_types=1);

namespace N11t\Holidays\Fake\Calculator;

use N11t\Holidays\Calculator\Calculator;
use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;

class FakeHolidayCalculator extends Calculator
{

    /**
     * @var HolidayCollection
     */
    private $holidayCollection;

    public function __construct(HolidayCollection $collection = null)
    {
        parent::__construct();

        if (!($collection instanceof HolidayCollection)) {
            $collection = new HolidayCollection();
        }

        $this->holidayCollection = $collection;
    }

    /**
     * {@inheritdoc}
     */
    protected function getHolidays(int $year): HolidayCollection
    {
        $holidays = array_filter($this->holidayCollection->toArray(), function (Holiday $holiday) use ($year) {
            $actualYear = ((int)$holiday->getDate()->format('Y'));
            return $actualYear === $year;
        });

        return new HolidayCollection(...$holidays);
    }
}

<?php
declare(strict_types=1);

namespace N11t\Holidays\Fake\Calculator\Provider;

use N11t\Holidays\Calculator\Provider\HolidayProvider;
use N11t\Holidays\Entity\Holiday;
use N11t\Holidays\Entity\HolidayCollection;

class FakeHolidayProvider implements HolidayProvider
{

    /**
     * @var HolidayCollection
     */
    private $holidayCollection;

    /**
     * FakeHolidayProvider constructor.
     * @param HolidayCollection $holidayCollection
     */
    public function __construct(HolidayCollection $holidayCollection)
    {
        $this->holidayCollection = $holidayCollection;
    }

    /**
     * {@inheritdoc}
     */
    public function getHolidays(int $year): HolidayCollection
    {
        $holidays = array_filter($this->holidayCollection->toArray(), function (Holiday $holiday) use ($year) {
            $actualYear = ((int)$holiday->getDate()->format('Y'));
            return $actualYear === $year;
        });

        return new HolidayCollection(...$holidays);
    }
}

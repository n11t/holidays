[![pipeline status](https://gitlab.com/n11t/holidays/badges/master/pipeline.svg)](https://gitlab.com/n11t/holidays/commits/master) [![coverage report](https://gitlab.com/n11t/holidays/badges/master/coverage.svg)](https://gitlab.com/n11t/holidays/commits/master)

# Holidays

Object oriented service to calculate holidays between given dates or check if given date is holiday.
Can be extended by own providers.

## Installation

Installing via Composer (recommended)

```
composer require n11t/holidays
```

## Usage

```php
<?php
require_once __DIR__ . '/vendor/autoload.php';

$calculator = new \N11t\Holidays\Country\Germany();

// Check if date is holiday
$calculator->isHoliday(new \DateTime('2018-12-24')); // True

// Get Holidays in Range
$calculator->between(new \DateTime('2018-03-26'), new \DateTime('2018-04-02')); // 2018-03-30, 2018-04-02

// Extend calculation by own providers
$calculator->isHoliday(new \DateTime('2018-03-01')); // False
$calculator->addProvider(new FirstOfMarchProvider());
$calculator->isHoliday(new \DateTime('2018-03-01')); // True

class FirstOfMarchProvider implements \N11t\Holidays\Calculator\Provider\HolidayProvider {
    public function getHolidays(int $year): \N11t\Holidays\Entity\HolidayCollection
    {
        $holidays = [
            new \N11t\Holidays\Entity\Holiday('2018-03-01', 'Funny Holiday')
        ];

        return new \N11t\Holidays\Entity\HolidayCollection(...$holidays);
    }
}
```

Tipp: Always use the `\N11t\Holidays\HolidayCalculator` interface as type-hint.


## Supported States

Currently Germany and it's states are Supported. Feel free to contribute.
